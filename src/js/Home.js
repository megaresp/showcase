import { setCookie } from "./lib/utils";
import NavAnimation from "./lib/NavAnimation";

const Home = () => {
  NavAnimation();

  function handleClick(e) {
    e.preventDefault();
    const href = `./${e.target.href.split("/").pop()}`;
    setCookie("hasRun", GlobalAppState.hasRun, 3600000);
    location.href = href;
  }

  return (
    <>
      <picture className="image-pizza">
        <source srcSet="./img/pizza.webp" />
        <img src="./img/pizza.jpg" alt="Pizza!" />
      </picture>
      <div className="wrap-start">
        <a
          onClick={handleClick}
          href="./choose"
          className="start button"
          type="button"
        >
          CHOOSE YOUR PIZZA
        </a>
      </div>
      <div className="home">
        <div className="strapline">
          <div className="i">
            <div>I</div>
          </div>
          <div className="want">
            <div>WANT</div>
          </div>
          <div className="pizza">
            <div>PIZZA</div>
          </div>
          <div className="now">
            <div>NOW!</div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Home;
