import { Link } from "react-router-dom";

const NotFound = () => {
  return (
    <div className="content" style={{ columnCount: 1 }}>
      <h1>404 Error: Page not found</h1>
      <p>
        Sorry, the page you’re looking for doesn’t exist.{" "}
        <Link to="/">Please click here</Link>.
      </p>
    </div>
  );
};

export default NotFound;
