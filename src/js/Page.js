import Header from "./component/Header";
import Main from "./component/Main";
import Footer from "./component/Footer";

const Page = (props) => {
  const { name } = props;

  return (
    <div className={`page ${name}`} style={{ overflowX: "hidden" }}>
      <Header name={name} />
      <Main name={name} />
      <Footer name={name} />
    </div>
  );
};

export default Page;
