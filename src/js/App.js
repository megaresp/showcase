import { StrictMode } from "react";
import { render } from "react-dom";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Page from "./Page";

const App = () => {
  return (
    <StrictMode>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Page name="home" />} />
          <Route path="/choose" element={<Page name="choose" />} />
          <Route path="/order" element={<Page name="order" />} />
          <Route path="/eat" element={<Page name="eat" />} />
          <Route path="/about" element={<Page name="about" />} />
          <Route path="/contact" element={<Page name="contact" />} />
          <Route path="*" element={<Page name="notfound" />} />
        </Routes>
      </BrowserRouter>
    </StrictMode>
  );
};

render(<App />, document.getElementById("root"));
