import NotFound from "../NotFound";
import Placeholder from "../Placeholder";
import Home from "../Home";

const Main = (props) => {
  const { name } = props;

  return (
    <main>
      <div className="constrain">
        {name === "notfound" ? (
          <NotFound name={name} />
        ) : name === "home" ? (
          <Home name={name} />
        ) : (
          <Placeholder name={name} />
        )}
      </div>
    </main>
  );
};

export default Main;
