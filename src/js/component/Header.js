import { useEffect, useState } from "react";
import { gsap } from "gsap";
import Nav from "./Nav";
import Menu from "./Menu";

const Header = (props) => {
  const { name } = props;

  const [isDown, setIsDown] = useState(false);
  const duration = 0.5;
  const tl = gsap.timeline();

  const animate = () => {
    if (isDown) {
      tl.seek(tl.totalDuration(), false);
      tl.timeScale(2).reverse();
    } else {
      tl.resume();
    }

    setIsDown(!isDown);
  };

  useEffect(() => {
    const mediaQuery = window.matchMedia("(max-width: 640px)");

    let topX = "15px";
    let topY = "0px";
    let bottomX = "-15px";
    let bottomY = "-50px";

    if (mediaQuery.matches) {
      topX = "7.5px";
      topY = "-1px";
      bottomX = "-15px";
      bottomY = "-35px";
    }

    tl.fromTo(
      ".nav .middle",
      { opacity: 1, duration: duration },
      { opacity: 0, duration: duration }
    );
    tl.fromTo(
      ".nav .top",
      {
        x: 0,
        y: 0,
        rotation: 0,
        transformOrigin: "left top",
        duration: duration,
      },
      {
        x: topX,
        y: topY,
        rotation: 45,
        transformOrigin: "left top",
        duration: duration,
      },
      `>-=${duration}`
    );
    tl.fromTo(
      ".nav .bottom",
      {
        x: 0,
        y: 0,
        rotation: 0,
        transformOrigin: "right top",
        duration: duration,
      },
      {
        x: bottomX,
        y: bottomY,
        rotation: -45,
        transformOrigin: "right top",
        duration: duration,
      },
      "<"
    );
    tl.fromTo(
      ".menu",
      { x: "-100vw", duration: duration },
      { x: 0, duration: duration },
      "<"
    );
    tl.pause();
  }, [isDown]);

  return (
    <>
      <header className="py-3 text-white">
        <div className="constrain">
          <Nav animate={animate} />
        </div>
      </header>
      <Menu name={name} animate={animate} />
    </>
  );
};

export default Header;
