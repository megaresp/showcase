const Nav = (props) => {
  const { animate } = props;

  return (
    <div className="nav" onClick={animate}>
      <div className="top"></div>
      <div className="middle"></div>
      <div className="bottom"></div>
    </div>
  );
};

export default Nav;
