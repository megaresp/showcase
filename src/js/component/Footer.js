const Footer = () => {
  const dateObj = new Date();

  return (
    <footer>
      <div className="constrain">
        <div className="copyright">
          Copyright &copy; {dateObj.getFullYear()}, all rights reserved.
        </div>
      </div>
    </footer>
  );
};

export default Footer;
