import { Link } from "react-router-dom";

const Menu = (props) => {
  const { animate, name } = props;

  return (
    <div className="menu">
      <div className="inner">
        <Link
          onClick={animate}
          to="/"
          className={name === "home" ? "active" : ""}
        >
          Home
        </Link>
        <Link
          onClick={animate}
          to="/choose"
          className={name === "choose" ? "active" : ""}
        >
          Choose
        </Link>
        <Link
          onClick={animate}
          to="/order"
          className={name === "order" ? "active" : ""}
        >
          Order
        </Link>
        <Link
          onClick={animate}
          to="/eat"
          className={name === "eat" ? "active" : ""}
        >
          Eat!
        </Link>
        <Link
          onClick={animate}
          to="/about"
          className={name === "about" ? "active" : ""}
        >
          About
        </Link>
        <Link
          onClick={animate}
          to="/contact"
          className={name === "contact" ? "active" : ""}
        >
          Contact
        </Link>
      </div>
    </div>
  );
};

export default Menu;
