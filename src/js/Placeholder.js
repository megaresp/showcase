import { Link } from "react-router-dom";
import { Ucfirst } from "./lib/utils";

const Placeholder = (props) => {
  const { name } = props;

  return (
    <div className="content">
      <h1>Welcome to the “{Ucfirst(name)}” placeholder</h1>
      <div className="columns">
        <p>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis
          sapiente voluptatum <strong>blanditiis nihil</strong> delectus,
          corrupti quaerat optio omnis amet quos!
        </p>
        <p>
          Lorem, <a href="/about">click this link</a>, then ipsum dolor sit amet
          consectetur adipisicing elit. Iste, sint.
        </p>
        <p>
          Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sunt,
          aliquam. Ratione nulla asperiores at! Architecto deleniti quam ullam
          recusandae quas odit temporibus reprehenderit placeat!
        </p>
        <h3>Lorem ipsum dolor sit amet.</h3>
        <p>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Laudantium,
          quas in? Amet commodi rerum consequatur quis voluptatibus.
        </p>
        <p>
          Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sed, quod?
        </p>
        <p>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Fuga
          distinctio veniam cum! Autem quaerat ratione saepe sint laudantium.
        </p>
        <h3>Lorem ipsum dolor sit amet consectetur.</h3>
        <p>
          Lorem ipsum dolor sit, amet consectetur adipisicing elit.
          Reprehenderit, voluptatum? Facilis, dolore molestiae, recusandae, sed
          quas a accusantium dolor qui omnis enim animi!
        </p>
        <p>
          Lorem ipsum dolor sit, amet consectetur adipisicing elit. Natus vitae
          quisquam molestiae nam ab asperiores neque assumenda cumque. Maiores,
          fugiat quisquam!
        </p>
        <p>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis
          sapiente voluptatum blanditiis nihil delectus, corrupti quaerat optio
          omnis amet quos!
        </p>
        <h3>Consectetur adipisicing elit! Et tu?</h3>
        <p>
          Lorem ipsum dolor sit, amet consectetur adipisicing elit. Natus vitae
          quisquam molestiae nam ab asperiores neque assumenda cumque. Maiores,
          fugiat quisquam!
        </p>
        <p>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis
          sapiente voluptatum <strong>blanditiis nihil</strong> delectus,
          corrupti quaerat optio omnis amet quos!
        </p>
        <p>
          Lorem, ipsum dolor sit amet consectetur adipisicing elit. Iste, sint.
        </p>
        <p>
          Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sunt,
          aliquam. Ratione nulla asperiores at! Architecto deleniti quam ullam
          recusandae quas odit temporibus reprehenderit placeat!
        </p>
        <h3>Lorem ipsum dolor sit amet consectetur</h3>
        <p>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Sunt saepe
          sint quam?
        </p>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing?</p>
        <p>
          Lorem ipsum dolor sit amet consectetur, adipisicing elit. Debitis odit
          velit provident odio, at illum molestiae! Accusantium, distinctio
          repudiandae adipisci et aliquam quod? Quae cum atque veniam natus quo!
        </p>
      </div>
    </div>
  );
};

export default Placeholder;
