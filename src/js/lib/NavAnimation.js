import { useEffect } from "react";
import { gsap } from "gsap";
import { getCookie } from "../lib/utils";

const NavAnimation = () => {
  const tl1 = gsap.timeline();
  const duration = 1.0;
  const easeType = "bounce.out";

  const quake = (times) => {
    const tl2 = gsap.timeline();
    tl2.to(".start.button", {
      scale: 1.075,
      rotateZ: 1,
      duration: 0.04,
    });
    tl2.to(
      ".start.button",
      {
        scale: 0.97,
        rotateZ: -1,
        duration: 0.05,
        ease: easeType,
      },
      ">"
    );
    tl2.to(
      ".start.button",
      {
        scale: 1,
        rotateZ: 0,
        duration: 0.03,
        ease: easeType,
      },
      ">"
    );
    tl2.repeat(times - 1);
  };

  useEffect(() => {
    if (GlobalAppState.hasRun) return;

    const hasRun = getCookie("hasRun");
    if (hasRun) return;

    GlobalAppState.hasRun = true;

    tl1.fromTo(
      ".home .i",
      { x: "-200vw" },
      { x: 0, duration: duration, ease: easeType },
      0.25
    );
    tl1.fromTo(
      ".home .want",
      { x: "100vw" },
      { x: 0, duration: duration, ease: easeType },
      `-=0.4`
    );
    tl1.fromTo(
      ".home .pizza",
      {
        x: 0,
        y: "-100vh",
        rotationZ: 0,
        duration: 1,
      },
      {
        x: 0,
        y: 0,
        rotationZ: -360,
        duration: duration * 1.5,
        ease: easeType,
      },
      `<-=0.6`
    );
    tl1.fromTo(
      ".home .now",
      { x: "-100vh" },
      { x: 0, duration: duration, ease: easeType },
      `<+=0.3`
    );
    tl1.fromTo(
      ".image-pizza",
      { x: "100vw", opacity: 0 },
      { x: 0, opacity: 1, duration: duration * 1.25, ease: "power2.out" },
      duration
    );
    tl1.fromTo(
      ".start.button",
      { opacity: 0 },
      { opacity: 1, duration: duration * 3.5, ease: "power2.out" },
      ">-=1.5"
    );
    tl1.call(quake, [3]);
  }, []);
};

export default NavAnimation;
