export const Ucfirst = (str) => {
  return `${str.charAt(0).toUpperCase()}${str.substr(1)}`;
};

export const getCookie = (name) => {
  name += "=";
  const decodedCookie = decodeURIComponent(document.cookie);
  const ca = decodedCookie.split(";");
  for (let i = 0; i < ca.length; i++) {
    let c = ca[i];
    while (c.charAt(0) == " ") {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
};

export const setCookie = (name, value, millseconds) => {
  const d = new Date();
  d.setTime(d.getTime() + millseconds);
  let expires = "expires=" + d.toUTCString();
  document.cookie = name + "=" + value + ";" + expires + ";path=/";
};
