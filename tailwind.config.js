module.exports = {
  mode: "jit",
  content: ["./src/**/*.{html,js}"],
  theme: {
    extend: {
      colors: {
        black: "rgb(16, 16, 16)",
        white: "rgb(239, 239, 239)",
        brand: "rgb(255, 127, 0)",
        alt: "rgb(255, 177, 0)",
        accent: "rgb(5, 178, 218)",
        alert: "rgb(255, 30, 0)",
        background: "rgba(black, 0.95)",
      },
    },
    screens: {
      ss: "280px",
      xs: "350px",
      sm: "640px",
      md: "768px",
      lg: "1024px",
      xl: "1280px",
      xx: "1536px",
    },
  },
  variant: {},
  plugins: [],
};
