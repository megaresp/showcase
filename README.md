# Frontend Showcase
A showcase of my frontend work built using React, Tailwind, PostCSS and SCSS.
## Developer Notes

To install this project locally...
* ```git clone https://gitlab.com/megaresp/showcase.git```
* ```cd showcase```
* ```npm install```

## Viewing the showcase
* ```npm run dev```
* [http://localhost:1234](http://localhost:1234)

## Working on the game
* Load the ```showcase/``` folder in your favourite editor
* Start working on files in ```~/src/```
